package com.serg.createmessageactivity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView

class CreateMessageActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_message)
    }
    //asdvvb
    fun onSendMessage(view: View) {
        val messageView: TextView = findViewById(R.id.message)
        val messageText = messageView.text.toString()
        val intent = Intent(this, ReceiveMessageActivity::class.java)
        intent.putExtra(ReceiveMessageActivity.EXTRA_MESSAGE, messageText)
        startActivity(intent)
        ////
    }
}