package com.serg.createmessageactivity

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class ReceiveMessageActivity : Activity() {

    companion object {
        const val EXTRA_MESSAGE = "message";
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive_message)
        val intent = intent
        val messageText = intent.getStringExtra(EXTRA_MESSAGE)
        val messageView: TextView = findViewById(R.id.message);
        messageView.setText(messageText)
    }


}